//
//  HDVideo.h
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@interface HDVideo : NSObject

@property (nonatomic, strong) NSString *videoID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, assign) int viewCount;
@property (nonatomic, strong) NSString *imageUrlString;
@property (nonatomic, strong) UIImage *videoImage;
@end
