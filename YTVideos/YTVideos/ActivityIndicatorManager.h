//
//  ActivityIndicatorManager.h
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityIndicatorManager : NSObject

+ (void)addNetworkTask;
+ (void)minusNetworkTask;
+ (void)showActivityIndicator;
+ (void)dismissActivityIndicator;

@end
