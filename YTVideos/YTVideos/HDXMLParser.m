//
//  HDXMLParser.m
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import "AlertViewManager.h"
#import "ActivityIndicatorManager.h"
#import "HDXMLParser.h"

#import "HDVideo.h"

@interface HDXMLParser ()
{
    NSMutableArray *_newVideoList;
    void(^_completionBlock)(NSArray *newVideoList);
    HDVideo *_currentVideo;
    NSString *_currentElementName;
    NSString *_currentElementValue;
}
@end

@implementation HDXMLParser


- (instancetype)initWithCompletionBlock:(void(^)(NSArray *newVideoList))completionBlock
{
    FNLog();
    
    self = [super init];
    if (self) {
        _completionBlock = completionBlock;
    }
    return self;
}

#pragma mark - NSXMLParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    FNLog();
    [ActivityIndicatorManager addNetworkTask];
    _newVideoList = [NSMutableArray array];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    //FNLog();
    
    _currentElementName = elementName;
    
    if ([_currentElementName isEqualToString:@"entry"]) {
        
        // Create new video object
        _currentVideo = [[HDVideo alloc] init];
        
    } else if ([_currentElementName isEqualToString:@"link"]) {
        
        // Check if type is text/html
        NSString *typeString = [attributeDict objectForKey:@"type"];
        if ([typeString isEqualToString:@"text/html"]) {
            
            // Check if urlString exists
            NSString *urlString = [attributeDict objectForKey:@"href"];
            if (urlString) {
                [_currentVideo setUrlString:urlString];
            }

        }
    } else if ([_currentElementName isEqualToString:@"yt:statistics"]) {
        [_currentVideo setViewCount:[[attributeDict objectForKey:@"viewCount"] intValue]];
    } else if ([_currentElementName isEqualToString:@"media:thumbnail"]) {
        // Check if thumbnail height is 90 (there are thumbnails of other heights which we dont' want)
        NSString *height = [attributeDict objectForKey:@"height"];
        if ([height isEqualToString:@"90"]) {
            [_currentVideo setImageUrlString:[attributeDict objectForKey:@"url"]];
        }
    }
    
    //NSLog(@"Processing element name: %@", elementName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([_currentElementName isEqualToString:@"title"]) {
        _currentElementValue = string;
        //NSLog(@"Processing elemant value: %@ for name: %@", _currentElementValue, _currentElementName);
    } else if ([_currentElementName isEqualToString:@"id"]) {
        _currentElementValue = string;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    //FNLog();
    
    if ([elementName isEqualToString:@"entry"]) {
        [_newVideoList addObject:_currentVideo];
        _currentVideo = nil;
    } else if ([elementName isEqualToString:@"title"]) {
        [_currentVideo setTitle:_currentElementValue];
    } else if ([elementName isEqualToString:@"id"]) {
        // Assuming the video id will be the last 11 characters in this property
        NSRange range = NSMakeRange([_currentElementValue length] - 11, 11);
        NSString *videoID = [_currentElementValue substringWithRange:range];
        [_currentVideo setVideoID:videoID];
    }

    //NSLog(@"%@",[NSString stringWithFormat:@"Did end element name %@", elementName]);
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    FNLog();
    
    [ActivityIndicatorManager minusNetworkTask];
    _completionBlock(_newVideoList);
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    FNLog();
    
    [AlertViewManager showAlertViewWithTitle:@"XML Parsing Error" message:[parseError localizedDescription]];
}



@end
