//
//  ActivityIndicatorManager.m
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

@import UIKit;

#import "ActivityIndicatorManager.h"

@interface ActivityIndicatorManager ()
{
    int _networkTaskCount;
}
@property (nonatomic, strong) UIView *activityIndicatorOverlayView;
@end

@implementation ActivityIndicatorManager

#pragma mark - Private Methods

+ (ActivityIndicatorManager *)sharedManager
{
    FNLog();
    
    static ActivityIndicatorManager *sharedManager = nil;
    static dispatch_once_t token;
    if (!sharedManager) {
        dispatch_once(&token, ^{
            sharedManager = [[super alloc] init];
        });
    }
    return sharedManager;
}

- (instancetype)init
{
    FNLog();
    
    self = [super init];
    if (self) {
        // NOTE: Actvity indicator view is no longer used in this project
        
        /*
        // Create semi-opaque gray background overlay
        _activityIndicatorOverlayView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [_activityIndicatorOverlayView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.5]];
        
        // Create activity indicator
        UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicatorView startAnimating];
        [_activityIndicatorOverlayView addSubview:activityIndicatorView];
        [activityIndicatorView setCenter:[_activityIndicatorOverlayView center]];
        */
    }
    return self;
}

- (void)addNetworkTask
{
    FNLog();
    
    // Increment task count
    _networkTaskCount ++;
    // Show network activity indicator
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)minusNetworkTask
{
    FNLog();
    
    // Decrement task count
    _networkTaskCount --;
    // If networkTaskCount = 0, all network activity has finished, hide network activity indicator
    if (_networkTaskCount == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

#pragma mark - Public Methods

+ (void)addNetworkTask
{
    FNLog();
    
    // Run method on singleton object
    [[ActivityIndicatorManager sharedManager] addNetworkTask];
}

+ (void)minusNetworkTask
{
    FNLog();
    
    // Run method on singleton object
    [[ActivityIndicatorManager sharedManager] minusNetworkTask];
}

// NOTE: This method is no longer used in this project
+ (void)showActivityIndicator
{
    FNLog();
    // Get activity indicator overlay view from shared manager
    UIView *activityIndicatorOverlayView = [[ActivityIndicatorManager sharedManager] activityIndicatorOverlayView];
    
    // Get root view
    UIView *rootView = [[[[[UIApplication sharedApplication] delegate] window] rootViewController] view];
    
    // Ensure graphic update is done on main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        // Overlay actvity indicator with semi opaque background over root view
        [rootView addSubview:activityIndicatorOverlayView];
    });
}

// Note: This method is no longer used in this project
+ (void)dismissActivityIndicator
{
    FNLog();
    
    // Get activity indicator overlay view from shared manager
    UIView *activityIndicatorOverlayView = [[ActivityIndicatorManager sharedManager] activityIndicatorOverlayView];
    
    // Ensure graphic update is done on main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        // Remove activity indicator overlay view from superview
        [activityIndicatorOverlayView removeFromSuperview];
    });
}

@end
