//
//  HDVideoPlayerView.h
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HDVideoPlayerViewDelegate <NSObject>
//- (void)playButtonTapped;
- (void)exitButtonTapped;
@end

@interface HDVideoPlayerView : UIView
@property (nonatomic, strong) UIWebView *playerView;
- (instancetype)initWithFrame:(CGRect)frame delegate:(id<HDVideoPlayerViewDelegate>)delegate;
//- (void)showLoadingScreen;
//- (void)showPlayButton;
@end
