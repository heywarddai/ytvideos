//
//  HDVideoListCell.h
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HDVideo;

@interface HDVideoListCell : UITableViewCell
- (void)layoutCellWithVideoInfo:(HDVideo *)video andFrame:(CGRect)frame;
@end
