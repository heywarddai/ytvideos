//
//  HDRootViewController.m
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import "HDRootViewController.h"

#import "HDRootView.h"

@interface HDRootViewController ()
{
    HDRootView *_rootView;
    
    HDVideoListController *_videoListController;
    HDVideoPlayer *_videoPlayer;
}
@end

@implementation HDRootViewController

- (void)loadView
{
    FNLog();
    _rootView = [[HDRootView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self setView:_rootView];
}

- (void)viewDidLoad {
    FNLog();
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Add video list view whose frame is pre-positioned in HDRootView
    CGRect videoListViewBounds = [[_rootView videoListView] bounds];
    _videoListController = [[HDVideoListController alloc] initWithFrame:videoListViewBounds delegate:self];
    [self addChildViewController:_videoListController];
    [_videoListController didMoveToParentViewController:self];
    [[_rootView videoListView] addSubview:[_videoListController view]];
    
    // Add player view whose frame is pre-positioned in HDRootView
    CGRect playerViewBounds = [[_rootView playerView] bounds];
    _videoPlayer = [[HDVideoPlayer alloc] initWithFrame:playerViewBounds delegate:self];
    [self addChildViewController:_videoPlayer];
    [_videoPlayer didMoveToParentViewController:self];
    [[_rootView playerView] addSubview:[_videoPlayer view]];
}

#pragma mark - HDVideoListControllerDelegate

- (void)loadVideoWithVideoID:(NSString *)videoID
{
    FNLog();
    
    [_videoPlayer loadVideoWithVideoID:videoID];
    
    // Let root view know if player is showing to help it determine layout when rotation occurs
    [_rootView setIsShowingPlayer:YES];

    [UIView animateWithDuration:0.5 animations:^{
        // Hide the status bar
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        // Animate movingg the player and video list down
        [[_rootView playerView] setFrame:[_rootView expandedVideoPlayerFrame]];
        [[_rootView videoListView] setFrame:[_rootView retractedVideoListFrame]];
    }];
}

#pragma mark - HDVideoPlayerDelegate

- (void)hideVideoPlayer
{
    FNLog();
    
    // Let root view know if player is showing to help it determine layout when rotation occurs
    [_rootView setIsShowingPlayer:NO];
    
    [UIView animateWithDuration:0.5 animations:^{
        // Show the status bar
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        // Animate the moving the player and list up
        [[_rootView playerView] setFrame:[_rootView originalVideoPlayerFrame]];
        [[_rootView videoListView] setFrame:[_rootView originalVideoListFrame]];
    }];
}

#pragma mark - UIViewController Protocol

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark - Clean up

- (void)didReceiveMemoryWarning {
    FNLog();
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
