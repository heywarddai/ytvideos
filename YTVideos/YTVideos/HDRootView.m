//
//  HDRootView.m
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#define kPlayerHeightWidthRatio 9.0/16.0

#import "HDRootView.h"
@interface HDRootView ()
{
    float _playerViewHeight;
}
@end

@implementation HDRootView
@synthesize videoListView = _videoListView, playerView = _playerView;

#pragma mark - Init

- (instancetype) initWithFrame:(CGRect)frame
{
    FNLog();
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setAutoresizesSubviews:YES];
        
        // Calculate height in 16:9 ratio according to screen width
        _playerViewHeight = CGRectGetWidth([self frame]) * kPlayerHeightWidthRatio;

        _videoListView = [[UIView alloc] initWithFrame:[self originalVideoListFrame]];
        [self addSubview:_videoListView];
        [_videoListView setAutoresizesSubviews:YES];

        _playerView = [[UIView alloc] initWithFrame:[self originalVideoPlayerFrame]];
        [_playerView setBackgroundColor:[UIColor blackColor]];
        [_playerView setAutoresizesSubviews:YES];
        
        [self addSubview:_playerView];
    }
    return self;
}

#pragma mark - Public Methods

- (CGRect)originalVideoPlayerFrame
{
    FNLog();
    
    CGRect playerViewFrame;
    
    // If orientation is portrait or iOS 8 or above
    if (isIOS8orAbove() | !isLandscape()) {
        // Player view frame is as wide as self frame
        playerViewFrame = CGRectMake(0.0, -_playerViewHeight, CGRectGetWidth([self frame]), _playerViewHeight);
    } else {
        // If orientation is landscape and iOS version is below 8
        // Reverse self frame height and width
        playerViewFrame = CGRectMake(0.0, -_playerViewHeight, CGRectGetHeight([self frame]), _playerViewHeight);
    }
    return playerViewFrame;
}

- (CGRect)originalVideoListFrame
{
    FNLog();
    
    float statusBarHeight = CGRectGetHeight([self statusBarFrame]);
    CGRect videoListViewFrame;
    
    // If orientation is portrait or iOS 8 or above
    if (isIOS8orAbove() | !isLandscape()) {
        // Video list frame is below status bar and as wide as self frame
        videoListViewFrame = CGRectMake(0.0, statusBarHeight, CGRectGetWidth([self frame]), CGRectGetHeight([self frame]) - statusBarHeight);
    } else {
        // If orientation is landscape and iOS version is below 8
        // Reverse self frame height and width
        videoListViewFrame = CGRectMake(0.0, statusBarHeight, CGRectGetHeight([self frame]), CGRectGetWidth([self frame]) - statusBarHeight);
    }
    return videoListViewFrame;
}

- (CGRect)expandedVideoPlayerFrame
{
    FNLog();
    
    CGRect expandedVideoPlayerFrame;
    
    // If orientation is portrait or iOS 8 or above
    if (isIOS8orAbove() | !isLandscape()) {
        // Video list frame is below status bar and as wide as self frame
        expandedVideoPlayerFrame = CGRectMake(0.0, 0.0, CGRectGetWidth([self frame]), _playerViewHeight);
    } else {
        // If orientation is landscape and iOS version is below 8
        // Reverse self frame height and width
        expandedVideoPlayerFrame = CGRectMake(0.0, 0.0, CGRectGetHeight([self frame]), _playerViewHeight);
    }
    return expandedVideoPlayerFrame;
}

- (CGRect)retractedVideoListFrame
{
    FNLog();
    
    CGRect retractedVideoListFrame;
    // If orientation is portrait or iOS 8 or above
    if (isIOS8orAbove() | !isLandscape()) {
        // Video list frame is below status bar and as wide as self frame
        retractedVideoListFrame = CGRectMake(0.0, _playerViewHeight, CGRectGetWidth([self frame]), CGRectGetHeight([self frame]) - _playerViewHeight);
    } else {
        // If orientation is landscape and iOS version is below 8
        // Reverse self frame height and width
        retractedVideoListFrame = CGRectMake(0.0, _playerViewHeight, CGRectGetHeight([self frame]), CGRectGetWidth([self frame]) - _playerViewHeight);
    }
    return retractedVideoListFrame;
}

#pragma mark - Layout

- (void)layoutSubviews
{
    FNLog();

    [super layoutSubviews];
    
    [self animateLayout];
}

- (void)animateLayout
{
    FNLog();
    
    // Animate layout change
    [UIView animateWithDuration:0.5 animations:^{
        if (!_isShowingPlayer) {
            [_videoListView setFrame:[self originalVideoListFrame]];
            [_playerView setFrame:[self originalVideoPlayerFrame]];
        } else {
            [_videoListView setFrame:[self retractedVideoListFrame]];
            [_playerView setFrame:[self expandedVideoPlayerFrame]];
        }
    }];
    
}

// Status bar frame height and width needs to be reversed in landscape in iOS 7 or lower
- (CGRect)statusBarFrame
{
    FNLog();
    
    CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
    if (!isLandscape() | isIOS8orAbove()) {
        return statusBarFrame;
    } else {
        return CGRectMake(0.0, 0.0, CGRectGetHeight(statusBarFrame), CGRectGetWidth(statusBarFrame));
    }
}

// Test whether device os is iOS 8 or above.
// NOTE: This will not different between version updates
BOOL isIOS8orAbove()
{
    int iOSVersion = [[[UIDevice currentDevice] systemVersion] intValue];
    return iOSVersion >= 8;
}

// Determine interface orientation according to status bar orientation
BOOL isLandscape()
{
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
