//
//  UrlSessionManager.m
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import "UrlSessionManager.h"

#import "AlertViewManager.h"
#import "ActivityIndicatorManager.h"

@interface UrlSessionManager ()
@property (nonatomic, strong) NSMutableDictionary *urlCompletionHandlerDict;
@end

@implementation UrlSessionManager

#pragma mark - Public Method

+ (void)startDownloadTaskWithUrlString:(NSString *)urlString completionHandler:(void (^)(NSData *))completionHandler
{
    FNLog();
    
    NSURL *url = [NSURL URLWithString:[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    // Save the completion handler using its corresponding URL
    [[[UrlSessionManager sharedManager] urlCompletionHandlerDict] setObject:completionHandler forKey:urlString];
    // Start data task and activate completion handler when task completes
    NSURLSessionDownloadTask *downloadTask = [[UrlSessionManager sharedSession] downloadTaskWithURL:url];
    
    [ActivityIndicatorManager addNetworkTask];
    [downloadTask resume];
}

#pragma mark - Private Method
+ (UrlSessionManager *)sharedManager
{
    FNLog();
    
    // Create singleton shared manager object
    static UrlSessionManager *sharedManager = nil;
    static dispatch_once_t token;
    if (!sharedManager) {
        dispatch_once(&token, ^{
            sharedManager = [[super alloc] init];
        });
    }
    return sharedManager;
}

- (instancetype)init
{
    FNLog();
    
    self = [super init];
    if (self) {
        
        _urlCompletionHandlerDict = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (NSURLSession *)sharedSession
{
    FNLog();
    
    // Create singleton shared session object with self as delegate and current queue as delegate queue
    static NSURLSession *sharedSession = nil;
    static dispatch_once_t token;
    if (!sharedSession) {
        dispatch_once(&token, ^{
            NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            // Create a background concurrent queue
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [queue setMaxConcurrentOperationCount:2];
            
            sharedSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:[UrlSessionManager sharedManager] delegateQueue:queue];
        });
    }
    return sharedSession;
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler
{
    FNLog();
    
    // Display error if response status code is not 200
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if ([httpResponse statusCode] != 200) {
        NSString *errorMessage = [NSString stringWithFormat:@"Url: %@\nHTTP Status Code: %li", [[[dataTask currentRequest] URL] absoluteString], (long)[httpResponse statusCode]];
        [AlertViewManager showAlertViewWithTitle:@"Data Task Error" message:errorMessage];
    }
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    FNLog();
    
    if (error) {
        // Display error description
        [AlertViewManager showAlertViewWithTitle:@"Data Task Error" message:[[error localizedDescription] stringByAppendingString:[NSString stringWithFormat:@"\n%@",[[[task currentRequest] URL] absoluteString]]]];
    }
    [ActivityIndicatorManager minusNetworkTask];
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    FNLog();
    
    // Retrieve completion handler
    NSString *urlString = [[[downloadTask currentRequest] URL] absoluteString];
    void(^completionHandler)(NSData *) = [[self urlCompletionHandlerDict] objectForKey:urlString];
    
    // Run completionHandler with data
    NSData *data = [NSData dataWithContentsOfURL:location];
    completionHandler(data);
}

@end
