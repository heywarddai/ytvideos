//
//  ColorHeader.h
//
//  Created by Heyward Dai on 10/20/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#ifndef ColorHeader_h
#define ColorHeader_h

#define kPomegranateColor [UIColor colorWithRed:0.96 green:0.3 blue:0.33 alpha:1.0]
#define kNeonAvocadoColor [UIColor colorWithRed:0.0 green:1.0 blue:0.4 alpha:1.0]
#define kCarribeanColor [UIColor colorWithRed:0.26 green:0.75 blue:0.95 alpha:1.0]
#define kGrayColor [UIColor colorWithRed:0.55 green:0.55 blue:0.55 alpha:1.0]
#define kLightGrayColor [UIColor colorWithRed:0.81 green:0.81 blue:0.81 alpha:1.0]

#endif