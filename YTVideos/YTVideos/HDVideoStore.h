//
//  HDVideoStore.h
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HDVideo;

@interface HDVideoStore : NSObject

+ (HDVideoStore *)sharedStore;

- (void)getJsonVideoList;
- (void)getXmlVideoList;
- (void)downloadVideoImageForVideo:(HDVideo *)video completionHandler:(void(^)())completionHandler;
- (NSArray *)videoList;

@end
