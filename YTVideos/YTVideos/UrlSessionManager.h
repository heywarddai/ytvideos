//
//  UrlSessionManager.h
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrlSessionManager : NSObject <NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>

+ (void)startDownloadTaskWithUrlString:(NSString *)urlString completionHandler:(void(^)(NSData *))completionHandler;

@end
