//
//  HDRootView.h
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDRootView : UIView
@property BOOL isShowingPlayer;
@property (nonatomic, strong) UIView *videoListView;
@property (nonatomic, strong) UIView *playerView;
- (CGRect)originalVideoPlayerFrame;
- (CGRect)originalVideoListFrame;
- (CGRect)expandedVideoPlayerFrame;
- (CGRect)retractedVideoListFrame;
@end
