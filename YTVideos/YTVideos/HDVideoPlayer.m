//
//  HDVideoPlayer.m
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

@import MediaPlayer;

#import "HDVideoPlayer.h"

#import "AlertViewManager.h"

@interface HDVideoPlayer ()
{
    __weak id<HDVideoPlayerDelegate> _delegate;
    
    HDVideoPlayerView *_playerView;
    CGRect _frame;
}
@end

@implementation HDVideoPlayer

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame delegate:(id<HDVideoPlayerDelegate>)delegate
{
    FNLog();
    
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _delegate = delegate;
        _frame = frame;
    }
    return self;
}

- (void)loadView
{
    FNLog();
    
    _playerView = [[HDVideoPlayerView alloc] initWithFrame:_frame delegate:self];
    [self setView:_playerView];
}

- (void)viewDidLoad {
    FNLog();
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // MPMoviePlayerController will not play YouTube videos. The recommended way is to embed the YouTube video into an UIWebView and present that instead
    /*
    _moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:nil];
    [_moviePlayerController setControlStyle:MPMovieControlStyleNone];
    [[_moviePlayerController view] setFrame:[[_playerView playerView] bounds]];
    [[_playerView playerView] addSubview:[_moviePlayerController view]];
    */
}

- (void)viewWillAppear:(BOOL)animated
{
    FNLog();
    
    [super viewWillAppear:animated];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMoviePlayerPlaybackDidFinishNotification:) name:MPMoviePlayerPlaybackDidFinishNotification object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMoviePlayerLoadStateDidChangeNotification) name:MPMoviePlayerLoadStateDidChangeNotification object:_moviePlayerController];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMoviePlayerDidExitFullScreenNotification) name:MPMoviePlayerDidExitFullscreenNotification object:_moviePlayerController];
     */
}

#pragma mark - Public method

- (void)loadVideoWithVideoID:(NSString *)videoID
{
    FNLog();
    
    // This javascript code is copied from online.. not sure how it works
    static NSString *youTubeVideoHTML = @"<!DOCTYPE html><html><head><style>body{margin:0px 0px 0px 0px;}</style></head> <body> <div id=\"player\"></div> <script> var tag = document.createElement('script'); tag.src = \"http://www.youtube.com/player_api\"; var firstScriptTag = document.getElementsByTagName('script')[0]; firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); var player; function onYouTubePlayerAPIReady() { player = new YT.Player('player', { width:'%0.0f', height:'%0.0f', videoId:'%@', events: { 'onReady': onPlayerReady, } }); } function onPlayerReady(event) {  } </script> </body> </html>";
    NSString* html = [NSString stringWithFormat:youTubeVideoHTML, CGRectGetWidth([[_playerView playerView] frame]), CGRectGetHeight([[_playerView playerView] frame]), videoID];
    // NOTE: baseURL cannot be nil or else webview won't load
    [[_playerView playerView] loadHTMLString:html baseURL:[[NSBundle mainBundle] resourceURL]];
}

#pragma mark - HDVideoPlayerViewDelegate

- (void)exitButtonTapped
{
    FNLog();
    
    [_delegate hideVideoPlayer];
}

#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark - Clean up

- (void)didReceiveMemoryWarning {
    FNLog();
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
