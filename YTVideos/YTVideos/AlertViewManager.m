//
//  AlertViewManager.m
//
//  Created by Heyward Dai on 10/13/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#define kDestructiveActionKey @"Destructive Action Key"
#define kActionBlocksKey @"Action Blocks Key"

#import "AlertViewManager.h"

@interface AlertViewManager ()
@property (nonatomic, strong) NSMutableDictionary *actionSheetActionsDict;
@end

@implementation AlertViewManager
@synthesize actionSheetActionsDict = _actionSheetActionsDict;

#pragma mark - Init
+ (AlertViewManager *)sharedManager
{
    FNLog();
    
    // Create singleton shared manager objet
    static AlertViewManager *sharedManager = nil;
    if (!sharedManager) {
        sharedManager = [[super allocWithZone:nil] init];
    }
    return sharedManager;
}

// Prohibit this class from allocing twice
+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    FNLog();
    
    return [self sharedManager];
}

- (instancetype)init
{
    FNLog();
    
    self = [super init];
    if (self) {
        _actionSheetActionsDict = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - Method

+ (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    FNLog();
    
    // If UIAlertController exists (ie. iOS 8 or later)
    if ([UIAlertController class]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
        // Present alert from root view of main window
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alertController animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
}

+ (void)showActionSheetWithTitle:(NSString *)title message:(NSString *)message desctructiveButtonTitle:(NSString *)destructiveButtonTitle desctructiveAction:(void(^)())destructiveAction buttonTitles:(NSArray *)buttonTitles actionBlocks:(NSArray *)actionBlocks;
{
    FNLog();
    
    // If UIAlertController class exists (ie. iOS 8 or above)
    if ([UIAlertController class]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        if (destructiveButtonTitle) {
            
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                destructiveAction();
            }];
            [alertController addAction:alertAction];
        }
        for (int i = 0; i < [buttonTitles count]; i++) {
            
            NSString *buttonTitle = [buttonTitles objectAtIndex:i];
            void (^actionBlock)() = [actionBlocks objectAtIndex:i];
            
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                actionBlock();
            }];

            [alertController addAction:alertAction];
        }
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alertController animated:YES completion:nil];
    } else {
        AlertViewManager *sharedManager = [AlertViewManager sharedManager];
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:sharedManager cancelButtonTitle:@"Cancel" destructiveButtonTitle:destructiveButtonTitle otherButtonTitles:nil];
        for (NSString *buttonTitle in buttonTitles) {
            [actionSheet addButtonWithTitle:buttonTitle];
        }
        // Save actions as blocks with keys in a dictionary to be retrieved later
        NSDictionary *actionsDict = [NSDictionary dictionaryWithObjectsAndKeys:destructiveAction, kDestructiveActionKey, actionBlocks, kActionBlocksKey, nil];
        [[sharedManager actionSheetActionsDict] setObject:actionsDict forKey:[actionSheet title]];
        [actionSheet showInView:[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view]];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    FNLog();
    
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        // Retrieve actions dictionary by using title as key
        NSDictionary *actionDict = [[self actionSheetActionsDict] objectForKey:[actionSheet title]];
        // If button tapped was the destructive button
        if (buttonIndex == [actionSheet destructiveButtonIndex]) {
            // Retrieve the destructive action block from the actions dictionary and execute it
            void (^destructiveActionBlock)() = [actionDict objectForKey:kDestructiveActionKey];
            destructiveActionBlock();
        } else {
            // Retrieve other actions array
            NSArray *actionBlocks = [actionDict objectForKey:kActionBlocksKey];
            // Find the action corresponding the button pressed
            // NOTE: buttonIndex includes destructive and cancel button
            void (^actionBlock)() = [actionBlocks objectAtIndex:buttonIndex - 1];
            actionBlock();
        }
    }
    // Dismiss action sheet no matter which button was pressed. Therefore we don't need an action for the cancel button
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

@end
