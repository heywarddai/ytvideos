//
//  HDVideoListController.h
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HDVideoListControllerDelegate <NSObject>
- (void)loadVideoWithVideoID:(NSString *)videoID;
@end

@interface HDVideoListController : UITableViewController

- (instancetype)initWithFrame:(CGRect)frame delegate:(id<HDVideoListControllerDelegate>)delegate;

@end