//
//  AlertViewManager.h
//
//  Created by Heyward Dai on 10/13/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

@import CoreFoundation;
@import UIKit;

// This class is created to accomodate for the transition from UIAlertView into UIAlertController introduced in iOS 8
@interface AlertViewManager : NSObject <UIActionSheetDelegate>

+ (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;
+ (void)showActionSheetWithTitle:(NSString *)title message:(NSString *)message desctructiveButtonTitle:(NSString *)destructiveButtonTitle desctructiveAction:(void(^)())destructiveAction buttonTitles:(NSArray *)buttonTitles actionBlocks:(NSArray *)actionBlocks;

@end