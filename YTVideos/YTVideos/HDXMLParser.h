//
//  HDXMLParser.h
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDXMLParser : NSObject <NSXMLParserDelegate>

- (instancetype)initWithCompletionBlock:(void(^)(NSArray *newVideoList))completionBlock;

@end
