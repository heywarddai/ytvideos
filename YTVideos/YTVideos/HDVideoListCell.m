//
//  HDVideoListCell.m
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#define kMargin 10.0
#define kSpacing 10.0

#import "HDVideoListCell.h"

#import "UrlSessionManager.h"

#import "HDVideo.h"

@interface HDVideoListCell ()
{
    UILabel *_titleLabel;
    UILabel *_viewCountLabel;
    UIImageView *_videoImageView;
}
@end

@implementation HDVideoListCell

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    FNLog();
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Init the property and them as subviews
        [self setAutoresizesSubviews:YES];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_titleLabel setTextColor:kCarribeanColor];
        [_titleLabel setNumberOfLines:2];
        [_titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:19.0]];
        [self addSubview:_titleLabel];
        
        _viewCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_viewCountLabel setTextColor:kLightGrayColor];
        [_viewCountLabel setFont:[UIFont systemFontOfSize:14.0]];
        [self addSubview:_viewCountLabel];
        
        _videoImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self addSubview:_videoImageView];
        
    }
    return self;
}

- (void)layoutCellWithVideoInfo:(HDVideo *)video andFrame:(CGRect)frame
{
    FNLog();
    
    // Set video title
    [_titleLabel setText:[video title]];
    
    // Set video view count
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###,###,###,###k views"];
    NSString *viewCountString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[video viewCount]/1000.0]];
    [_viewCountLabel setText:viewCountString];
    
    // Refresh cell with video info regardless whether image has been downloaded for video
    // User gets visual response quicker this way
    if ([video videoImage]) {
        [_videoImageView setImage:[video videoImage]];
    } else {
        [_videoImageView setImage:nil];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)layoutSubviews
{
    FNLog();
    
    // Need to call super's method for some reason or tableview separators will be positioned wrongly
    [super layoutSubviews];
    
    // Position video image view to the left with margin, all the way from top to bottom
    CGRect videoImageViewFrame = CGRectMake(kMargin, 0.0, 120.0, CGRectGetHeight([self frame]));
    [_videoImageView setFrame:videoImageViewFrame];
    
    // Position title label left of video image view with spacing, all the way to right, half way from the top to bottom
    CGRect titleLabelFrame = CGRectMake(CGRectGetMaxX(videoImageViewFrame) + kSpacing, 0.0, CGRectGetWidth([self frame]) - ((kMargin * 2.0) + kSpacing + CGRectGetWidth(videoImageViewFrame)), CGRectGetHeight([self frame]) *3.0/4.0);
    [_titleLabel setFrame:titleLabelFrame];
    
    // Position view count label below title label
    CGRect viewCountLabelFrame = CGRectMake(CGRectGetMaxX(videoImageViewFrame) + kSpacing, CGRectGetHeight([self frame]) * 3.0/4.0, CGRectGetWidth([self frame]) - ((kMargin * 2.0) + kSpacing + CGRectGetWidth(videoImageViewFrame)), CGRectGetHeight([self frame]) * 1.0/4.0);
    [_viewCountLabel setFrame:viewCountLabelFrame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    FNLog();
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
