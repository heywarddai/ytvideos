//
//  HDVideoPlayer.h
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import "HDVideoPlayerView.h"

@protocol HDVideoPlayerDelegate <NSObject>
- (void)hideVideoPlayer;
@end

@interface HDVideoPlayer : UIViewController <HDVideoPlayerViewDelegate>
- (instancetype)initWithFrame:(CGRect)frame delegate:(id<HDVideoPlayerDelegate>)delegate;
- (void)loadVideoWithVideoID:(NSString *)videoID;
@end