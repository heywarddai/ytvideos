//
//  HDVideoStore.m
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#define kYouTubeSearchXmlUrlString @"https://gdata.youtube.com/feeds/api/videos?max-results=50&pretty-print=true&orderby=viewCount&start-index=%i"
#define kYouTubeSearchJsonUrlString @"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&key=%@"
#define kYouTubeServerKey @"AIzaSyDVN_UfuTBatJ8HnQ3SSemnlndLB8r5D50"

@import UIKit;

#import "HDVideoStore.h"
#import "UrlSessionManager.h"
#import "HDXMLParser.h"

#import "HDVideo.h"

@interface HDVideoStore ()
{
    int _startIndex;
    NSString *_nextPageToken;
    NSMutableArray *_videoList;
    HDXMLParser *_xmlParserDelegate;
}
@end

@implementation HDVideoStore

#pragma mark - Init

+ (HDVideoStore *)sharedStore
{
    FNLog();
    
    // Create singleton store object
    
    static HDVideoStore *sharedStore = nil;
    static dispatch_once_t token;
    
    if (!sharedStore)
    {
        dispatch_once(&token, ^{
            sharedStore = [[super alloc] init];
        });
    }
    return sharedStore;
}

- (instancetype)init
{
    FNLog();
    
    self = [super init];
    
    if (self) {
        _startIndex = 1;
        _videoList = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Public Methods
// NOTE: This uses the YouTube Data API v.2 which is deprecated
- (void)getXmlVideoList
{
    FNLog();
    
    NSString *urlString = [NSString stringWithFormat:kYouTubeSearchXmlUrlString, _startIndex];
    
    [UrlSessionManager startDownloadTaskWithUrlString:urlString completionHandler:^(NSData *data){
            [self parseXmlData:data];
            // Increment start index by 50
            _startIndex += 50;
    }];
}

// NOTE: This uses the current YouTube Data API v.3 but returns in JSON format
// NOTE: This method is not used for this project
- (void)getJsonVideoList
{
    FNLog();
        
    NSString *urlString = [NSString stringWithFormat:kYouTubeSearchJsonUrlString, kYouTubeServerKey];
    // If page token exists, add that to the end of the urlString
    if (_nextPageToken) {
        NSString *pageTokenString = [NSString stringWithFormat:@"&pageToken=%@", _nextPageToken];
        urlString = [urlString stringByAppendingString:pageTokenString];
    }
    
    [UrlSessionManager startDownloadTaskWithUrlString:urlString completionHandler:^(NSData *data) {
            [self parseJsonData:data];
    }];

}

- (void)downloadVideoImageForVideo:(HDVideo *)video completionHandler:(void (^)())completionHandler
{
    FNLog();
    
    [UrlSessionManager startDownloadTaskWithUrlString:[video imageUrlString] completionHandler:^(NSData *data) {
            // Create image from data
            UIImage *image = [UIImage imageWithData:data];
            // Save image to video
            [video setVideoImage:image];
            // Run completion handler given
            completionHandler();
    }];
}

- (NSArray *)videoList
{
    FNLog();

    // return videoList as immutable array
    return [NSArray arrayWithArray:_videoList];
}

#pragma mark - Private Methods

- (void)parseXmlData:(NSData *)data
{
    FNLog();
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:data];
    _xmlParserDelegate = [[HDXMLParser alloc] initWithCompletionBlock:^(NSArray *newVideoList) {
        
        // Add videos from new list to existing list
        [_videoList addObjectsFromArray:newVideoList];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kVideoListDownloadedNotification object:nil];
    }];
    [xmlParser setDelegate:_xmlParserDelegate];
    
    // Note: _xmlParserDelegate needs to have a strong reference somewhere before running parser in the background queue or it will be released before the parser runs
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [xmlParser parse];
    });
}

- (void)parseJsonData:(NSData *)data
{
    FNLog();
    
    // Need to write JSon parser here!!
}

@end
