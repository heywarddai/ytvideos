//
//  HDVideoListController.m
//  YTVideos
//
//  Created by Heyward Dai on 10/27/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#define kVideoListCell @"HDVideoListCell"
#define kThumbnailHeight 90.0

#import "HDVideoListController.h"
#import "HDVideoPlayer.h"

#import "HDVideoListCell.h"

#import "HDVideoStore.h"

#import "HDVideo.h"

@interface HDVideoListController ()
{
    __weak id<HDVideoListControllerDelegate> _delegate;
    CGRect _frame;
    BOOL _updatingList;
}
@end

@implementation HDVideoListController

- (instancetype)initWithFrame:(CGRect)frame delegate:(id<HDVideoListControllerDelegate>)delegate
{
    FNLog();
    
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        _delegate = delegate;
        _frame = frame;
        _updatingList = NO;
        [[self tableView] registerClass:[HDVideoListCell class] forCellReuseIdentifier:kVideoListCell];
    }
    return self;
}

- (void)viewDidLoad {
    FNLog();
    
    [super viewDidLoad];
    
    // Readjust the frame of the tableview
    [[self tableView] setFrame:_frame];
    
    // Add self as notification obersver before getting xml video list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleVideoListDownloadedNotification) name:kVideoListDownloadedNotification object:nil];
    
    // Let shared video store get xml video list
    [[HDVideoStore sharedStore] getXmlVideoList];
    
    // Set updating list to YES to prevent another update while this update hasn't finished
    _updatingList = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    FNLog();
    
    [super viewWillAppear:animated];
    
    // Re-add self as notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleVideoListDownloadedNotification) name:kVideoListDownloadedNotification object:nil];
    // Hide the navigation bar when its view is showing
    [[self navigationController] setNavigationBarHidden:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    FNLog();
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    FNLog();
    
    // Return the number of rows in the section.
    return [[[HDVideoStore sharedStore] videoList] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FNLog();
    
    return kThumbnailHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FNLog();
    
    HDVideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:kVideoListCell forIndexPath:indexPath];

    // Configure the cell...
    
    HDVideo *video = [[[HDVideoStore sharedStore] videoList] objectAtIndex:[indexPath row]];
    
    // Layout cell when whatever video info we have
    [cell layoutCellWithVideoInfo:video andFrame:[cell bounds]];
    
    // If video does not have an imaged downloaded, download the image and refresh the cell afterwards
    if (![video videoImage]) {
        [[HDVideoStore sharedStore] downloadVideoImageForVideo:video completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell layoutCellWithVideoInfo:video andFrame:[cell bounds]];
            });
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FNLog();
    
    // Retrieve video
    HDVideo *video = [[[HDVideoStore sharedStore] videoList] objectAtIndex:[indexPath row]];

    // Load the video
    [_delegate loadVideoWithVideoID:[video videoID]];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Determine if scrollview has been scrolled to the bottom
    // NOTE: Maximum possible offset is the content size height minus the height of the view frame
    if ([scrollView contentOffset].y > ([scrollView contentSize].height - CGRectGetHeight(_frame))) {
        // Only update the list if it is not begin currently updated
        if (!_updatingList) {
            // Toggle this BOOL to in case user scrolled to the bottom again before updating is finished
            _updatingList = YES;
            [[HDVideoStore sharedStore] getXmlVideoList];
        }
    }
}

#pragma mark - Notification Handler

- (void)handleVideoListDownloadedNotification
{
    FNLog();
    
    // NOTE: Could try to update only the new cells
    // Reload entire table on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self tableView] reloadData];
        _updatingList = NO;
    });
}

#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark - Clean up

- (void)viewWillDisappear:(BOOL)animated
{
    FNLog();
    
    [super viewWillDisappear:animated];
    
    // Deregister notification center
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    FNLog();
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
