//
//  LogHeader.h
//  GolfTracker
//
//  Created by Heyward Dai on 9/25/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#ifndef SmartGolf_LogHeader_h
#define SmartGolf_LogHeader_h
    #define FNLog() DBLog(@"%@ running '%@'", [self class], NSStringFromSelector(_cmd))
    #ifdef DEBUG
        #define DBLog(...) NSLog(__VA_ARGS__)
    #else
        #define DBLog(...)
    #endif
#endif