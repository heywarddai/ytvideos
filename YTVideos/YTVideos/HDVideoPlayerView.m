    //
//  HDVideoPlayerView.m
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#define kMargin 20.0
#define kButtonSize 40.0

#import "HDVideoPlayerView.h"
#import "YTPlayerView.h"

@interface HDVideoPlayerView ()
{
    __weak id<HDVideoPlayerViewDelegate> _delegate;
    UIButton *_exitButton;
}
@end;

@implementation HDVideoPlayerView
@synthesize playerView = _playerView;

- (instancetype)initWithFrame:(CGRect)frame delegate:(id<HDVideoPlayerViewDelegate>)delegate
{
    FNLog();
    
    self = [super initWithFrame:frame];
    if (self) {
        
        // Auto resizing mask has to be set so rotation will trigger layout subviews
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [self setAutoresizesSubviews:YES];
        [self setBackgroundColor:[UIColor blackColor]];
        
        _delegate = delegate;
        
        // Setup UIWebView to play youtube video
        _playerView = [[UIWebView alloc] initWithFrame:[self bounds]];
        [_playerView setContentMode:UIViewContentModeCenter];
        [_playerView setMediaPlaybackAllowsAirPlay:NO];
        [_playerView setMediaPlaybackRequiresUserAction:YES];
        [[_playerView scrollView] setBounces:NO];
        [[_playerView scrollView] setScrollEnabled:NO];
        [_playerView setCenter:[self center]];
        [_playerView setAutoresizesSubviews:YES];
        [self addSubview:_playerView];
        
        // Position exit button at the top left corner of the screen
        _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_exitButton addTarget:_delegate action:@selector(exitButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [_exitButton setFrame:CGRectMake(kMargin, kMargin, kButtonSize, kButtonSize)];
        [_exitButton setTitle:@"X" forState:UIControlStateNormal];
        [_exitButton setTitleColor:kCarribeanColor forState:UIControlStateNormal];
        [[_exitButton titleLabel] setFont:[UIFont boldSystemFontOfSize:20.0]];
        [self addSubview:_exitButton];
    }
    return self;
}

- (void)layoutSubviews
{
    FNLog();
    
    [super layoutSubviews];
    
    [_playerView setCenter:[self center]];
}

@end
