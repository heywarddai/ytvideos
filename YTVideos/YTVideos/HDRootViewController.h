//
//  HDRootViewController.h
//  YTVideos
//
//  Created by Heyward Dai on 10/28/14.
//  Copyright (c) 2014 Heyward Dai. All rights reserved.
//

#import "HDVideoListController.h"
#import "HDVideoPlayer.h"

@interface HDRootViewController : UIViewController <HDVideoListControllerDelegate, HDVideoPlayerDelegate>

@end